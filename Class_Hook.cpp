#include <windows.h>
#include <stdio.h>
#include "psapi.h"
class DtHook
{
public:
	DWORD pAddrS, pAddrToJumpS;
	byte *bBuff[5];
	void __stdcall CreateJmp(DWORD dwTo, DWORD dwFrom);
	void __stdcall SetHook();
	void __stdcall RemoveHook();
	void __stdcall InstallMyHooks(char* Dll,char* Function,DWORD Addr);
}; 

void __stdcall DtHook::CreateJmp(DWORD dwTo, DWORD dwFrom)
{
  if(!IsBadReadPtr((void*)dwFrom, 1))
  *((byte*)((DWORD*)dwFrom)) = 0xe9; // JMP 
  if(!IsBadReadPtr((void*)(dwFrom+1), 4))
  *((DWORD*)(dwFrom+1)) = (dwTo-dwFrom-5);
}

void __stdcall DtHook::SetHook()
{
  DWORD dwOP;
  VirtualProtect((void*)pAddrS, 5, PAGE_EXECUTE_READWRITE, &dwOP);
  CopyMemory(bBuff, (void*)pAddrS, 5);
  CreateJmp(pAddrToJumpS, pAddrS);
  VirtualProtect((void*)pAddrS, 5, dwOP, &dwOP);
}

void __stdcall DtHook::RemoveHook()
{
  DWORD dwOP;
  VirtualProtect((void*)pAddrS, 5, PAGE_EXECUTE_READWRITE, &dwOP);
  CopyMemory((void*)pAddrS, bBuff, 5);
  VirtualProtect((void*)pAddrS, 5, dwOP, &dwOP);
}

void __stdcall DtHook::InstallMyHooks(char* Dll,char* Function,DWORD Addr)
{
  HINSTANCE hUser32;
  hUser32 =  LoadLibrary(Dll);
  if(hUser32 == 0) return;
  pAddrS = (DWORD)GetProcAddress(hUser32, Function);
  if(pAddrS == 0) return;
  pAddrToJumpS = Addr;
  SetHook();
}

